#!/bin/sh

# qtile autostart file
# created by Raven

# load Xresources settings
xrdb -merge $HOME/.Xresources &

# start dunst (notifications)
dunst &

# start picom (compositor)
picom -b &

# set cursor theme and size
xsetroot -xcf /usr/share/icons/Capitaine-Cursors-Dark/cursors/left_ptr 34 &

# set background image
feh --bg-fill /usr/share/backgrounds/img1.png &

# start xclip clipboard manager
xclip -silent &

# start network manager applet
nm-applet &

# start blueman applet
blueman-applet &
